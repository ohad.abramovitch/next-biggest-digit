"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.nextBigger = exports.isNumberAscending = void 0;

var nextBigger = function nextBigger(num) {
  var newNum = '';
  var stringNum = num + '';
  var numLength = stringNum.length;
  var pivotIndex = findFirstPivot(stringNum);

  if (pivotIndex === -1) {
    return -1;
  }

  newNum += stringNum.substring(0, pivotIndex - 1);
  var firstDigitToSwap = stringNum[pivotIndex - 1];
  var secondPart = '';
  var secondDigitToSwapIndex;
  var foundSecondDigit = false;

  for (secondDigitToSwapIndex = pivotIndex; !foundSecondDigit && secondDigitToSwapIndex < numLength; secondDigitToSwapIndex++) {
    if (stringNum[secondDigitToSwapIndex] <= firstDigitToSwap) {
      foundSecondDigit = true;
    }
  }

  if (foundSecondDigit) {
    secondDigitToSwapIndex--;
  }

  var secondDigitToSwap = stringNum[secondDigitToSwapIndex - 1];
  newNum += secondDigitToSwap;
  secondPart = stringNum.substring(pivotIndex, numLength);
  secondPart = secondPart.replace(secondDigitToSwap, '');
  secondPart = secondPart.split('').reverse().join('');
  var secondPartIndex;
  var digitPlaced = false;

  for (secondPartIndex = 0; !digitPlaced && secondPartIndex < secondPart.length; secondPartIndex++) {
    if (secondPart[secondPartIndex] > firstDigitToSwap) {
      digitPlaced = true;
      newNum += firstDigitToSwap;
    }

    newNum += secondPart[secondPartIndex];
  }

  newNum += secondPart.substring(secondPartIndex);

  if (!digitPlaced) {
    newNum += firstDigitToSwap;
  }

  return Number(newNum);
};

exports.nextBigger = nextBigger;

var findFirstPivot = function findFirstPivot(stringNum) {
  var foundFirstPivot = false;
  var pivotIndex = stringNum.length - 1;

  for (; !foundFirstPivot && pivotIndex >= 1; pivotIndex--) {
    if (stringNum[pivotIndex] > stringNum[pivotIndex - 1]) {
      return pivotIndex;
    }
  }

  if (!foundFirstPivot) {
    return -1;
  }
};

var isNumberAscending = function isNumberAscending(num) {
  num += '';

  for (var i = 0; i < num.length - 1; i++) {
    if (num[i] > num[i + 1]) {
      return false;
    }
  }

  return true;
};

exports.isNumberAscending = isNumberAscending;