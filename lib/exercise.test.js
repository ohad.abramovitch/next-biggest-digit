"use strict";

var _exercise = require("./exercise.js");

describe('testing isNumberAscending function', function () {
  test('one digit number', function () {
    expect((0, _exercise.isNumberAscending)(2)).toBe(true);
  }), test('repeating number', function () {
    expect((0, _exercise.isNumberAscending)(777)).toBe(true);
  });
  test('ascending number', function () {
    expect((0, _exercise.isNumberAscending)(23689)).toBe(true);
  });
  test('descending number with last digit out of order', function () {
    expect((0, _exercise.isNumberAscending)(23680)).toBe(false);
  });
  test('descending number with digit out of order', function () {
    expect((0, _exercise.isNumberAscending)(23089)).toBe(false);
  });
  test('descending number with first digit out of order', function () {
    expect((0, _exercise.isNumberAscending)(523689)).toBe(false);
  });
});
describe('testing nextBigger function', function () {
  test('one digit number', function () {
    expect((0, _exercise.nextBigger)(3)).toBe(-1);
  });
  test('two digit number descending', function () {
    expect((0, _exercise.nextBigger)(54)).toBe(-1);
  });
  test('two digit number ascending', function () {
    expect((0, _exercise.nextBigger)(89)).toBe(98);
  });
  test('repeating two-digit number', function () {
    expect((0, _exercise.nextBigger)(33)).toBe(-1);
  });
  test('repeating number', function () {
    expect((0, _exercise.nextBigger)(44444)).toBe(-1);
  });
  test('multiple digit number descending', function () {
    expect((0, _exercise.nextBigger)(65432)).toBe(-1);
  });
  test('multiple digit number ascending', function () {
    expect((0, _exercise.nextBigger)(12345)).toBe(12354);
  });
  test('random number', function () {
    expect((0, _exercise.nextBigger)(534976)).toBe(536479);
  });
  test('number where second digit to swap is adjacent to first digit to swap', function () {
    expect((0, _exercise.nextBigger)(1234321)).toBe(1241233);
  });
});