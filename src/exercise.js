export {isNumberAscending, nextBigger}

const nextBigger = (num) => {
    let newNum = '';
    const stringNum = num + ''
    const numLength = stringNum.length;
    const pivotIndex = findFirstPivot(stringNum)
    if (pivotIndex === -1) {
        return -1;
    }
    newNum += stringNum.substring(0,pivotIndex);
    const firstDigitToSwap = stringNum[pivotIndex];
    let secondPart = '';
    let secondDigitToSwapIndex;

    let foundSecondDigit = false;
    for (secondDigitToSwapIndex = pivotIndex + 1; !foundSecondDigit && secondDigitToSwapIndex < numLength ;secondDigitToSwapIndex++) {
        
        if (stringNum[secondDigitToSwapIndex] <= firstDigitToSwap) {
            foundSecondDigit = true
        }
    }
    if (foundSecondDigit) {
        secondDigitToSwapIndex--
    }
    const secondDigitToSwap = stringNum[secondDigitToSwapIndex - 1];
    newNum += secondDigitToSwap
    secondPart = stringNum.substring(pivotIndex + 1, numLength);
    secondPart = secondPart.replace(secondDigitToSwap, '');
    secondPart = secondPart.split('').reverse().join('');
    let secondPartIndex;
    let digitPlaced = false
    for (secondPartIndex = 0;!digitPlaced && secondPartIndex<secondPart.length;secondPartIndex++) {
        if (secondPart[secondPartIndex] > firstDigitToSwap) {
            digitPlaced = true;
            newNum += firstDigitToSwap;
        }
        newNum += secondPart[secondPartIndex];
    }
    
    newNum += secondPart.substring(secondPartIndex,)
    if (!digitPlaced) {
        newNum += firstDigitToSwap
    }
    return Number(newNum)
};

const findFirstPivot = (stringNum) => {
    let pivotIndex = stringNum.length - 1;
    for(; pivotIndex >= 1 ; pivotIndex--)
    {
        if (stringNum[pivotIndex] > stringNum[pivotIndex - 1]) {
            return pivotIndex--;
        }
    }
    return -1;
}




