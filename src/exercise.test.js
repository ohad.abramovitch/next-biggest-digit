
import {isNumberAscending, nextBigger} from './exercise.js';

describe('testing nextBigger function', () => {
    test('one digit number', () =>{
        expect(nextBigger(3)).toBe(-1);
    })
    test('two digit number descending', () => {
        expect(nextBigger(54)).toBe(-1);
    })
    test('two digit number ascending', () => {
        expect(nextBigger(89)).toBe(98);
    })
    test('repeating two-digit number', () => {
        expect(nextBigger(33)).toBe(-1);
    })
    test('repeating number', () => {
        expect(nextBigger(44444)).toBe(-1);
    })
    test('multiple digit number descending', () => {
        expect(nextBigger(65432)).toBe(-1);
    })
    test('multiple digit number ascending', () => {
        expect(nextBigger(12345)).toBe(12354);
    })
    test('random number', () => {
        expect(nextBigger(534976)).toBe(536479);
    })
    test('number where second digit to swap is adjacent to first digit to swap', () => {
        expect(nextBigger(1234321)).toBe(1241233)
    })
})

